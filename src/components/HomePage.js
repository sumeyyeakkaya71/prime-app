import React, { useRef } from 'react';
import { SlideMenu } from 'primereact/slidemenu';


const HomePage = () => {
    const menu = useRef(null);
    const items = [
        {
            label: 'Dashboard',
            icon: 'pi pi-fw pi-home'
        },
        {
            label:'Nodes',
            icon:'pi pi-fw pi-server',
            items:[
                {
                    label:'Add Node',
                    icon:'pi pi-fw pi-plus',
                },
                {
                    label:'Delete',
                    icon:'pi pi-fw pi-trash'
                },
                {
                    separator:true
                }
            ]
        },
        {
            label:'Services',
            icon:'pi pi-fw pi-align-justify',
            items:[

            ]
        },
        {
            label: 'Graphs',
            icon: 'pi pi-fw pi-chart-line',
            items:[

            ]
        },
        {
            label: 'Alerts',
            icon: 'pi pi-fw pi-bell',
            items:[

            ]
        },
        {
            separator:true
        },
     
    ];

    return (
        <div>
            <div className="card">
                <SlideMenu model={items} viewportHeight={800} menuWidth={175}></SlideMenu>

                <SlideMenu ref={menu} model={items} popup viewportHeight={220} menuWidth={175}></SlideMenu>
            </div>
        </div>
    );
}

export default HomePage;
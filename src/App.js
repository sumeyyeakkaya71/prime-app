import React from 'react';
import './App.css';
import "primereact/resources/themes/lara-light-indigo/theme.css";  //theme
import "primereact/resources/primereact.min.css";                  //core css
import "primeicons/primeicons.css"; 
import HomePage from './components/HomePage';
import Navbar from './components/Navbar';
import "bootstrap/dist/css/bootstrap.min.css";


function App() {

  return (
    <div className="App">
      <div className="row-navigation row m-0 p-0 w-100">
            <Navbar/>
            <br/>
   <HomePage/>
    </div>
    </div>
  );
}
export default App;
